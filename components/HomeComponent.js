import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Link from '../utilities/Link';

export default class HomeComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {

        return (
            <View>
                <View style={styles.bottomLine}>
                    <Text>Try searching for...</Text>
                </View>
                <View style={styles.bottomLine}>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View><Text>Bible References</Text></View>
                        <Link text="LIST ALL"></Link>
                    </View>
                    <Link text="Prov 25:2"></Link>
                    <Link text="Acts 13"></Link>
                    <Link text="John 3:16"></Link>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    bottomLine:{
        borderBottomWidth:5,
        borderBottomColor:'#707070',
    }

})


