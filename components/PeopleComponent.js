import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import TextRegular from '../utilities/TextRegular';
import TextBold from '../utilities/TextBold';
import { Card } from 'native-base';

export default class PeopleComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {

        return (
            <View>
                <Text style={{ marginBottom: 5 }}>People</Text>
                {this.props.people.map(person => {
                    // startEndArray = verse.verseText.split(this.props.query);
                    return (
                        <Card style={styles.personCard} key={"container" + person.slug}>
                            <View>
                                <Text style={styles.name}>{person.name}</Text>
                                <Text>
                                    {/* Some people don't have descriptions. In those cases, we should list the first few verses for that person */}
                                    {person.description && <TextRegular style={{ display: 'inline' }} text={person.description.substring(0,150) + "..."} />}
                                    {/*
                                        <TextBold style={{ display: 'inline' }} text={" " + this.props.query + " "}></TextBold>
                                        <TextRegular style={{ display: 'inline' }} text={startEndArray[1]}></TextRegular>
                                    */}
                                </Text>
                            </View>
                        </Card>
                    )
                })}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    personCard: {
        marginBottom: 15,
        borderRadius: 10,
        padding: 15
    },
    name: {
        color: 'blue',
        fontSize: 18,
        fontFamily: 'Roboto'
    },
})


