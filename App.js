import React, { Component, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { ApolloClient, HttpLink, InMemoryCache } from 'apollo-boost';
import { ApolloProvider, graphql, Query } from 'react-apollo';
import { onError } from "apollo-link-error";
import { ApolloLink } from 'apollo-link';
import PrincipalComponent from './components/PrincipalComponent';
import * as Font from 'expo-font';  

/*
class PeopleComponent extends Component {
  render() {
    return (
      <Query query={peopleQuery} variables={{ "input": "scriptures" }}>
        {(data, loading, error) => {
          if (loading) {
            return "Loading";
          }
          if (error) {
            console.log(error)
            return "error"
          }
          console.log(data)
          return "yayy"
        }}
      </Query>
    )
  }
}*/

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors)
    graphQLErrors.map(({ message, locations, path }) =>
      console.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
      ),
    );

  if (networkError) console.log(`[Network error]: ${networkError}`);
});
const httpLink = new HttpLink({
  uri: 'http://68.183.135.124:4000/',
  headers: {

  }
})
const link = ApolloLink.from([errorLink, httpLink]);

const client = new ApolloClient({
  link: link,
  cache: new InMemoryCache()
});

export default class App extends React.Component{
  state = {
    fontLoaded: false,
  };
  

  constructor(props) {
    super(props);
    this.state = {
    };
    this._isMounted = false;
  }

  async componentDidMount() {
    this._isMounted = true;
    await Font.loadAsync({
      "roboto-bold": require("./assets/fonts/Roboto-Bold.ttf"),
      "Roboto": require("./assets/fonts/Roboto-Regular.ttf"),
      "roboto-medium": require("./assets/fonts/Roboto-Medium.ttf")
    });
    if(this._isMounted)
      this.setState({ fontLoaded: true });
  }

  componentWillUnmount(){
    this._isMounted = false;
  }


  render() {
    return (
      <ApolloProvider client={client}>
      <View style={styles.container}>
      {this.state.fontLoaded ? (<PrincipalComponent client={client}></PrincipalComponent>):null}
      </View>  
    </ApolloProvider>
    );
  }


}


const styles = StyleSheet.create({
  container: {
    padding: 20,
    paddingTop: 50,
  },
});
