import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';

export default class TextRegular extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        
        return (
            <Text style={styles.text}>{this.props.text}</Text>
        );
    }
}

const styles = StyleSheet.create({
    text:{
        fontFamily: 'Roboto',
        fontSize: 16,
        textAlign:'justify'
    }
})